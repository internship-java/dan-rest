package md.codefactory.models;

import java.util.List;

import lombok.Getter;

import lombok.NoArgsConstructor;

import lombok.Setter;

@Getter @Setter @NoArgsConstructor

public class MangaResult {

    private List<Manga> result;

}
