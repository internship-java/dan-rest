package md.codefactory.services;

import java.util.List;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;

import md.codefactory.models.Manga;

import md.codefactory.models.MangaResult;

@Service

public class MangaService {

    Logger logger = LoggerFactory.getLogger(MangaService.class);

    private static final String MANGA_SEARCH_URL="http://api.jikan.moe/search/manga/";

    @Autowired

    RestTemplate restTemplate;

    public List<Manga> getMangasByTitle(String title) {

        return restTemplate.getForEntity(MANGA_SEARCH_URL+title, MangaResult.class).getBody().getResult();

    }

}